const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const uploadOnMemory = require("./middleware/uploadOnMemory");
const path = require("path");

const {
  ApplicationControllers,
  AuthenticationControllers,
  UsersController,
  ForumControllers,
  EventControllers,
  OrderController,
} = require("./controllers");

const {
  User,
  Forum,
  KategoriForum,
  Event,
  Order,
  Payment,
} = require("../database/models");

function apply(app) {
  // Model
  const userModel = User;
  const forumModel = Forum;
  const categoryForumModel = KategoriForum;
  const eventModel = Event;
  const orderModel = Order;
  const paymentModel = Payment;

  // Delarasi controler
  const applicationControllers = new ApplicationControllers();
  const eventControllers = new EventControllers({ eventModel });
  const UsersControllers = new UsersController({ userModel });
  const forumControllers = new ForumControllers({
    forumModel,
    userModel,
    categoryForumModel,
  });

  const authenticationControllers = new AuthenticationControllers({
    jwt,
    userModel,
    bcrypt,
  });

  const orderController = new OrderController({
    orderModel,
    eventModel,
    paymentModel,
  });

  //API Auth
  app.get("/", applicationControllers.handleGetRoot);
  app.post("/auth/login", authenticationControllers.handleLogin);
  app.post("/auth/register", authenticationControllers.handleRegister);
  app.get("/auth/", authenticationControllers.authenticateToken);

  // API User
  app.delete(
    "/users/:id",
    authenticationControllers.authorize(1),
    UsersControllers.handleDeleteUser
  );

  app.get(
    "/user/",
    authenticationControllers.authorize(3),
    UsersControllers.handleGetUserById
  );

  app.get(
    "/users/",
    authenticationControllers.authorize(1),
    UsersControllers.handleGetAllUsers
  );

  app.put(
    "/user/",
    authenticationControllers.authorize(3),
    UsersControllers.handleUpdateUser
  );

  //Forum
  app.post(
    "/forum/",
    authenticationControllers.authorize,
    forumControllers.handlePosting
  );

  app.post(
    "/forum/komen/",
    authenticationControllers.authorize,
    forumControllers.handleKomen
  );

  app.get("/forum/:id", forumControllers.handlegetForumWithReplies);
  app.get("/forum", forumControllers.handelGetForum);

  //Api Event
  app.post(
    "/event/create",
    authenticationControllers.authorize(1),
    uploadOnMemory,
    eventControllers.handleCreateEvent
  );
  app.put(
    "/event/:id",
    authenticationControllers.authorize(1),
    uploadOnMemory,
    eventControllers.handleCreateEvent
  );
  app.get("/event", eventControllers.handleGetAllEvents);
  app.get("/event/:id", eventControllers.handleGetEventById);
  app.get("/event/jenis/:id", eventControllers.handleGetEventById);

  // API Order
  app.post(
    "/order",
    authenticationControllers.authorize(3),
    orderController.handleCreate
  );
  app.get(
    "/order",
    authenticationControllers.authorize(1),
    orderController.handleGetAllOrders
  );
  app.get(
    "/payment",
    authenticationControllers.authorize(1),
    orderController.handleGetAllPayments
  );
  app.put(
    "/payment/:id",
    authenticationControllers.authorize(1),
    orderController.handleChangePaymentStatus
  );
  app.use(applicationControllers.handleError);
  return app;
}

module.exports = { apply };
