// uploadMiddleware.js
const multer = require("multer");
const path = require("path");
const fs = require("fs");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const uploadPath = "public/uploads/event";
    if (!fs.existsSync(uploadPath)) {
      fs.mkdirSync(uploadPath, { recursive: true });
    }
    cb(null, uploadPath);
  },
  filename: (req, file, cb) => {
    const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
    const fileExtension = path.extname(file.originalname);
    const fileName = file.fieldname + "-" + uniqueSuffix + fileExtension;
    cb(null, fileName);
  },
});

const upload = multer({
  storage: storage,
  fileFilter: (req, file, cb) => {
    const allowedFileTypes = /jpeg|jpg|png/;
    const extname = allowedFileTypes.test(
      path.extname(file.originalname).toLowerCase()
    );
    const mimetype = allowedFileTypes.test(file.mimetype);
    if (extname && mimetype) {
      return cb(null, true);
    } else {
      cb(
        "Hanya file gambar dengan format JPEG, JPG, atau PNG yang diperbolehkan."
      );
    }
  },
}).array("picture");

const uploadMiddleware = (req, res, next) => {
  upload(req, res, (err) => {
    if (err) {
      // Tangani kesalahan upload
      res.status(400).send(err);
    } else {
      const filePaths = req.files.map((file) =>
        path.join("uploads/event", file.filename)
      );
      console.log(filePaths);
      req.filePaths = filePaths;
      next();
    }
  });
};

module.exports = uploadMiddleware;
