require("dotenv").config();
const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const router = require("./router");

const app = express();

app.use(cors());
app.use(express.json());

module.exports = router.apply(app);
