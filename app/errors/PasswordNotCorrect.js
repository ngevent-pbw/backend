const ApplicationError = require("./ApplicationError");

class PasswordNotCorrect extends ApplicationError {
  constructor() {
    super("Password not correct!");
  }

  get details() {
    return {
      message: "Wrong password. Try again or click Forgot password to reset!",
    };
  }
}

module.exports = PasswordNotCorrect;
