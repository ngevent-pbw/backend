const EmailAlreadyTakenError = require("./EmailAlreadyTakenError");
const InvalidEmailAddress = require("./InvalidEmailAddress");
const UsernameAlreadyTakenError = require("./InvalidEmailAddress");
const EmailNotFound = require("./EmailNotFound");
const PasswordNotCorrect = require("./PasswordNotCorrect");

module.exports = {
  EmailAlreadyTakenError,
  InvalidEmailAddress,
  UsernameAlreadyTakenError,
  EmailNotFound,
  PasswordNotCorrect,
};
