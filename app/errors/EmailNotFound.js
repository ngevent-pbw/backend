const ApplicationError = require("./ApplicationError");

class EmailNotFound extends ApplicationError {
  constructor(email) {
    super(`${email} Email not found!`);
    this.email = email;
  }

  get details() {
    return { email: this.email };
  }
}

module.exports = EmailNotFound;
