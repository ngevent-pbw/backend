const ApplicationError = require("./ApplicationError");

class UsernameAlreadyTakenError extends UsernameAlreadyTakenError {
  constructor(username) {
    super(`${username} is already taken!`);
    this.username = username;
  }

  get details() {
    return { username: this.username };
  }
}

module.exports = UsernameAlreadyTakenError;
