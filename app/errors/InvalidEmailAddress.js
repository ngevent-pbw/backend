const ApplicationError = require("./ApplicationError");
class InvalidEmailAddress extends ApplicationError {
  constructor() {
    super("Invalid Entered Email Address");
  }
  get details() {
    return {
      message:
        "Valid e-mail can contain only latin letters, @ and . (Example : guest@gmail.com",
    };
  }
}
module.exports = InvalidEmailAddress;
