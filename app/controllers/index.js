const ApplicationControllers = require("./ApplicationControllers");
const AuthenticationControllers = require("./AuthenticationControllers");
const UsersController = require("./UsersController");
const ForumControllers = require("./ForumControllers");
const EventControllers = require("./EventControllers");
const OrderController = require("./OrderControllers");

module.exports = {
  ApplicationControllers,
  AuthenticationControllers,
  UsersController,
  ForumControllers,
  EventControllers,
  OrderController,
};
