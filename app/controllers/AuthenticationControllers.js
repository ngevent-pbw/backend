const ApplicationControllers = require("./ApplicationControllers");
const {
  EmailAlreadyTakenError,
  InvalidEmailAddress,
  EmailNotFound,
  PasswordNotCorrect,
} = require("../errors");

class AuthenticationControllers extends ApplicationControllers {
  constructor({ userModel, jwt, bcrypt }) {
    super();
    this.userModel = userModel;
    this.jwt = jwt;
    this.bcrypt = bcrypt;
  }

  authenticateToken = async (req, res, next) => {
    const authHeader = req.headers["authorization"];
    const token = authHeader && authHeader.split("Bearer ")[1];

    if (!token) {
      return res.sendStatus(401);
    }

    this.jwt.verify(token, "secretKey", (err, user) => {
      if (err) {
        return res.sendStatus(403);
      }

      req.user = user;
      res.sendStatus(200);
    });
  };

  authorize = (role) => async (req, res, next) => {
    const authHeader = req.headers["authorization"];
    console.log(authHeader);
    const token = authHeader && authHeader.split("Bearer ")[1];

    if (!token) {
      return res.sendStatus(401);
    }

    this.jwt.verify(token, "secretKey", (err, user) => {
      if (err) {
        return res.sendStatus(403);
      }
      if (role == 3) {
        req.user = user;
        next();
        return;
      }
      if (role != user.isAdmin) {
        return res.sendStatus(403);
      }
      req.user = user;
      next();
      return;
    });
  };

  handleRegister = async (req, res, next) => {
    try {
      const { Fname, Lname, password } = req.body;
      console.log(req.body);
      const email = req.body.email.toLowerCase();
      const emailContent = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (!email.match(emailContent)) {
        const err = new InvalidEmailAddress();
        res.status(401).json(err);
        return;
      }

      const existingEmail = await this.userModel.findOne({ where: { email } });
      if (existingEmail) {
        const err = new EmailAlreadyTakenError(email);
        res.status(422).json(err);
        return;
      }

      await this.userModel.create({
        name: Fname + " " + Lname,
        email,
        password: await this.encryptPassword(password),
        isAdmin: false,
      });

      res.status(201).json({ message: "User created successfully" });
    } catch (err) {
      next(err);
    }
  };

  handleLogin = async (req, res, next) => {
    try {
      const email = req.body.email.toLowerCase();
      const password = req.body.password;
      console.log(email, password);
      const emailContent = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (!email.match(emailContent)) {
        const err = new InvalidEmailAddress();
        res.status(401).json(err);
        return;
      }

      const checkEmail = await this.userModel.findOne({ where: { email } });
      if (!checkEmail) {
        const err = new EmailNotFound(email);
        res.status(422).json(err);
        return;
      }

      console.log(checkEmail.name);

      const verification = await this.verifyPassword(
        password,
        checkEmail.password
      );

      if (!verification) {
        const err = new PasswordNotCorrect();
        res.status(401).json(err);
        return;
      }

      const token = this.jwt.sign(
        {
          id: checkEmail.id,
          email: checkEmail.email,
          name: checkEmail.name,
          isAdmin: checkEmail.isAdmin,
        },
        "secretKey",
        { expiresIn: "1d" }
      );

      res.status(200).json({ message: "Suksess Login", token: token });
    } catch (err) {
      next(err);
    }
  };

  encryptPassword = async (password) => {
    return await this.bcrypt.hashSync(password, 10);
  };

  verifyPassword = async (password, encryptedPassword) => {
    return await this.bcrypt.compareSync(password, encryptedPassword);
  };
}

module.exports = AuthenticationControllers;
