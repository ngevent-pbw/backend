const ApplicationControllers = require("./ApplicationControllers");
// const {} = require("../errors");

class EventController extends ApplicationControllers {
  constructor({ eventModel }) {
    super();
    this.eventModel = eventModel;
  }

  handleCreateEvent = async (req, res) => {
    const {
      name,
      decs,
      jenis,
      penyelenggara,
      link,
      tipe,
      partisipan,
      isbBerbayar,
      harga,
      pendaftaran,
      tglEvent,
    } = req.body;
    const picture = req.filePaths;
    console.log(picture);

    try {
      const event = await this.eventModel.create({
        name,
        decs,
        jenis,
        picture,
        penyelenggara,
        link,
        tipe,
        partisipan,
        isbBerbayar,
        harga,
        pendaftaran,
        tglEvent,
        userId: req.user.id,
      });

      res.status(201).json({ event });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: error });
    }
  };

  handleEditEvent = async (req, res) => {
    const {
      name,
      decs,
      jenis,
      penyelenggara,
      link,
      tipe,
      partisipan,
      isbBerbayar,
      harga,
      pendaftaran,
      tglEvent,
    } = req.body;
    const picture = req.filePaths;

    try {
      const event = await this.eventModel.update(
        {
          name,
          decs,
          jenis,
          picture,
          penyelenggara,
          link,
          tipe,
          partisipan,
          isbBerbayar,
          harga,
          pendaftaran,
          tglEvent,
          userId: req.user.id,
        },
        {
          where: {
            id: req.params.id,
          },
        }
      );

      res.status(201).json({ event });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: error });
    }
  };

  handleGetAllEvents = async (req, res) => {
    const { limit = 10, page = 1 } = req.query;

    try {
      const offset = (page - 1) * limit;

      const { count, rows: events } = await this.eventModel.findAndCountAll({
        limit: parseInt(limit),
        offset: parseInt(offset),
      });

      const totalPages = Math.ceil(count / limit);

      res.status(200).json({ events, totalPages });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: error });
    }
  };

  handleGetEventById = async (req, res) => {
    const { id } = req.params;

    try {
      const event = await this.eventModel.findByPk(id);

      if (!event) {
        return res.status(404).json({ message: "Event not found" });
      }

      res.status(200).json({ event });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: error });
    }
  };

  handleGetEventsByJenis = async (req, res) => {
    const { jenis } = req.params;

    try {
      const events = await this.eventModel.findAll({
        where: {
          jenis: jenis,
        },
      });

      res.status(200).json({ events });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: error });
    }
  };
}

module.exports = EventController;
