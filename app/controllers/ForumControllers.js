const { Op } = require("sequelize");

const ApplicationControllers = require("./ApplicationControllers");

class ForumControllers extends ApplicationControllers {
  constructor({ forumModel, userModel, categoryForumModel }) {
    super();
    this.forumModel = forumModel;
    this.userModel = userModel;
    this.categoryForumModel = categoryForumModel;
  }

  handlePosting = async (req, res, next) => {
    const { judul, konten, kategoriId } = req.body;
    try {
      const forum = await this.forumModel.create({
        judul,
        konten,
        kategoriId,
        userId: req.user.id,
        tanggalDibuat: new Date(),
        tingkatKedalaman: 0,
        isPosting: true,
      });

      res.status(201).json({ forum });
    } catch (error) {
      console.error("Terjadi kesalahan saat membuat forum:", error);
      res.status(500).json({ message: error });
    }
  };

  handleKomen = async (req, res, next) => {
    const { konten, forumId } = req.body;

    try {
      const forumInduk = await this.forumModel.findByPk(forumId);
      const tingkatKedalaman = forumInduk.tingkatKedalaman + 1;
      const balasan = await this.forumModel.create({
        konten,
        idKomentarInduk: forumId,
        userId: req.user.id,
        tanggalDibuat: new Date(),
        tingkatKedalaman,
      });

      res.status(201).json({ balasan });
    } catch (error) {
      console.error("Terjadi kesalahan saat membuat balasan:", error);
      res
        .status(500)
        .json({ message: "Terjadi kesalahan saat membuat balasan" });
    }
  };

  handlegetForumWithReplies = async (req, res, next) => {
    const { id } = req.params;

    try {
      const forumInduk = await this.forumModel.findByPk(id);

      if (!forumInduk) {
        return res.status(404).json({ message: "Forum tidak ditemukan" });
      }

      const balasan = await this.getReplies(forumInduk.id);

      const forumDanBalasan = {
        forumInduk,
        balasan,
      };

      res.status(200).json(forumDanBalasan);
    } catch (error) {
      console.error(
        "Terjadi kesalahan saat mengambil forum dan balasan:",
        error
      );
      res.status(500).json({
        message: "Terjadi kesalahan saat mengambil forum dan balasan",
      });
    }
  };

  handelGetForum = async (req, res, next) => {
    const options = {
      attributes: {
        exclude: [
          "createdAt",
          "updatedAt",
          "idKomentarInduk",
          "updatedAt",
          "userId",
          "kategoriId",
          "isPosting",
        ],
      },
      include: [
        {
          model: this.userModel,
          attributes: ["id", "name", "email"],
          as: "user",
        },
        {
          model: this.categoryForumModel,
          attributes: ["id", "name"],
          as: "category",
        },
      ],
      order: [["createdAt", "DESC"]],
      where: [
        {
          isPosting: true,
        },
      ],
    };

    try {
      const forums = await this.forumModel.findAll({
        where: {
          isPosting: true,
        },
      });

      if (!forums) {
        return res.status(404).json({ message: "Forum tidak ditemukan" });
      }

      const forumData = [];

      for (const forum of forums) {
        const totalComments = await this.getTotalKomentar(forum.id);

        forumData.push({
          id: forum.id,
          judul: forum.judul,
          konten: forum.konten,
          totalKomentar: totalComments,
          createdAt: forum.createdAt,
        });
      }

      res.status(200).json(forumData);
    } catch (error) {
      console.error(
        "Terjadi kesalahan saat mengambil forum dan total komentar:",
        error
      );
      res.status(500).json({
        message: "Terjadi kesalahan saat mengambil forum dan total komentar",
      });
    }
  };

  getReplies = async (forumId) => {
    const balasan = await this.forumModel.findAll({
      where: { idKomentarInduk: forumId },
    });

    const replies = [];

    for (const komentar of balasan) {
      const sum = await this.getTotalKomentar(komentar.id);
      const subReplies = await this.getReplies(komentar.id);
      replies.push({
        komentar,
        balasan: sum,
        KomenBerikutnya: subReplies,
      });
    }

    return replies;
  };

  getTotalKomentar = async (forumId) => {
    const total = await this.forumModel.count({
      where: { idKomentarInduk: forumId },
    });

    return total;
  };
}

module.exports = ForumControllers;
