const ApplicationControllers = require("./ApplicationControllers");

class OrderController extends ApplicationControllers {
  constructor({ eventModel, orderModel, paymentModel }) {
    super();
    this.eventModel = eventModel;
    this.orderModel = orderModel;
    this.paymentModel = paymentModel;
  }

  handleCreate = async (req, res) => {
    const { idEvent, name, quantity } = req.body;
    try {
      const getUser = await this.eventModel.findByPk(idEvent);
      const totalHarga = quantity + getUser.harga;

      const pembayaran = await this.paymentModel.create({
        total: totalHarga,
        barcode: "pembayaran:" + totalHarga,
        status: false,
        pending: false,
      });
      const order = await this.orderModel.create({
        name: name,
        idEvent: idEvent,
        idPembayaran: pembayaran.id,
      });
      res.status(201).json({ order });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: error });
    }
  };

  handleGetAllPayments = async (req, res) => {
    const { page = 1, limit = 10 } = req.query;

    try {
      const options = {
        offset: (page - 1) * limit,
        limit: parseInt(limit),
      };

      const { count, rows: payments } = await this.paymentModel.findAndCountAll(
        options
      );

      const totalPages = Math.ceil(count / limit);

      res.status(200).json({ payments, totalPages });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: error });
    }
  };

  handleGetAllOrders = async (req, res) => {
    const { page = 1, limit = 10 } = req.query;

    try {
      const options = {
        offset: (page - 1) * limit,
        limit: parseInt(limit),
      };

      const { count, rows: orders } = await this.orderModel.findAndCountAll(
        options
      );

      const totalPages = Math.ceil(count / limit);

      res.status(200).json({ orders, totalPages });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: error });
    }
  };

  handleChangePaymentStatus = async (req, res) => {
    const { id } = req.params;

    try {
      const payment = await this.paymentModel.findByPk(id);

      if (!payment) {
        return res.status(404).json({ message: "Payment not found" });
      }

      payment.status = true;
      await payment.save();

      res.status(200).json({ message: "Payment status updated" });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: error });
    }
  };
}

module.exports = OrderController;
