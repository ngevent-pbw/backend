const ApplicationControllers = require("./ApplicationControllers");
// const {} = require("../errors");

class UsersController extends ApplicationControllers {
  constructor({ userModel }) {
    super();
    this.userModel = userModel;
  }

  handleDeleteUser = async (req, res, next) => {
    try {
      const deleteUser = await this.userModel.destroy({
        where: {
          id: req.params.id,
        },
      });
      if (deleteUser == 1) {
        res.status(200).json({ Message: "User deleted successfully" });
      }
    } catch (err) {
      next(err);
    }
  };

  handleGetUserById = async (req, res, next) => {
    const user = req.user.id;
    try {
      const getUser = await this.userModel.findByPk(user);
      res.status(200).json({
        data: {
          id: getUser.id,
          name: getUser.name,
          email: getUser.email,
          isAdmin: getUser.isAdmin,
          noHp: getUser.noHp,
        },
      });
    } catch (err) {
      next(err);
    }
  };

  handleUpdateUser = async (req, res, next) => {
    const { noHp, name } = req.body;
    try {
      const updateUser = await this.userModel.update(
        { name, noHp },
        {
          where: {
            id: req.user.id,
          },
        }
      );
      if (updateUser) {
        res.status(200).json({
          data: {
            id: req.params.id,
            name,
            noHp,
          },
        });
      }
    } catch (err) {
      next(err);
    }
  };

  handleGetAllUsers = async (req, res, next) => {
    const { page, limit } = req.query;
    const pageNumber = parseInt(page) || 1;
    const pageSize = parseInt(limit) || 10;
    const offset = (pageNumber - 1) * pageSize;

    try {
      const { count, rows } = await this.userModel.findAndCountAll({
        offset,
        limit: pageSize,
      });

      const totalPages = Math.ceil(count / pageSize);

      res.status(200).json({
        data: rows,
        meta: {
          totalUsers: count,
          totalPages,
          currentPage: pageNumber,
          pageSize,
        },
      });
    } catch (err) {
      next(err);
    }
  };
}

module.exports = UsersController;
