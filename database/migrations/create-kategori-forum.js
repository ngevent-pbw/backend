"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("kategori_forums", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      nama: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      // tambahkan kolom lain yang dibutuhkan
    });

    // tambahkan kode untuk memasukkan data awal ke tabel "KategoriForum" jika diperlukan
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("kategori_forums");
  },
};
