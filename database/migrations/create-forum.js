"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("forums", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      idKomentarInduk: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      isPosting: {
        type: Sequelize.BOOLEAN,
      },
      judul: {
        type: Sequelize.STRING,
      },
      konten: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      tingkatKedalaman: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      kategoriId: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      userId: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("forums");
  },
};
