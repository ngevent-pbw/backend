const { KategoriForum } = require("./kategori-forum");

const kategoriForumSeeder = async () => {
  try {
    await KategoriForum.bulkCreate([
      { nama: "Pembayaran" },
      { nama: "Refund" },
      { nama: "Ide" },
      { nama: "Pelatihan" },
    ]);
    console.log("KategoriForum seeder berhasil dijalankan.");
  } catch (error) {
    console.error(
      "Terjadi kesalahan saat menjalankan KategoriForum seeder:",
      error
    );
  }
};

module.exports = kategoriForumSeeder;
