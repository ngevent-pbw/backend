"use strict";
const { DataTypes, Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class KategoriForum extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // KategoriForum.hasMany(models.Forum, {
      //   foreignKey: "kategoriId",
      // });
    }
  }
  KategoriForum.init(
    {
      nama: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "kategori-forum",
    }
  );
  return KategoriForum;
};
