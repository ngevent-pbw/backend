"use strict";
const { DataTypes, Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class Payment extends Model {
    static associate(models) {
      // Forum.belongsTo(models.User, { foreignKey: "userId" });
      // Forum.belongsTo(models.KategoriForum, { foreignKey: "kategoriId" });
    }
  }
  Payment.init(
    {
      total: DataTypes.INTEGER,
      barcode: DataTypes.TEXT,
      status: DataTypes.BOOLEAN,
      pending: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "Payment",
    }
  );
  return Payment;
};
