"use strict";
const { DataTypes, Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class Forum extends Model {
    static associate(models) {
      // Forum.belongsTo(models.User, { foreignKey: "userId" });
      // Forum.belongsTo(models.KategoriForum, { foreignKey: "kategoriId" });
    }
  }
  Forum.init(
    {
      idKomentarInduk: DataTypes.INTEGER,
      isPosting: DataTypes.BOOLEAN,
      judul: DataTypes.STRING,
      konten: DataTypes.TEXT,
      tingkatKedalaman: DataTypes.INTEGER,
      kategoriId: DataTypes.INTEGER,
      userId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Forum",
    }
  );
  return Forum;
};
