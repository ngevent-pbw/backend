"use strict";
const { DataTypes, Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    static associate(models) {
      // Forum.belongsTo(models.User, { foreignKey: "userId" });
      // Forum.belongsTo(models.KategoriForum, { foreignKey: "kategoriId" });
    }
  }
  Order.init(
    {
      name: DataTypes.JSON,
      idEvent: DataTypes.INTEGER,
      idPembayaran: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Order",
    }
  );
  return Order;
};
