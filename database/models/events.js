"use strict";
const { DataTypes, Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Event extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // User.hasMany(models.Forum, {
      //   foreignKey: "userId",
      // });
    }
  }
  Event.init(
    {
      name: DataTypes.STRING,
      decs: DataTypes.TEXT,
      picture: DataTypes.JSON,
      jenis: DataTypes.STRING,
      picture: DataTypes.JSON,
      penyelenggara: DataTypes.STRING,
      link: DataTypes.STRING,
      tipe: DataTypes.STRING,
      partisipan: DataTypes.STRING,
      isbBerbayar: DataTypes.INTEGER,
      harga: DataTypes.INTEGER,
      pendaftaran: DataTypes.DATE,
      tglEvent: DataTypes.DATE,
      userId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Event",
    }
  );
  return Event;
};
